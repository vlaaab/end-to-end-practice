const { By, Builder, until } = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
require('dotenv').config();

// Set Chrome options to run in headless mode
const chromeOptions = new chrome.Options();
chromeOptions.addArguments('--headless');
chromeOptions.addArguments('--no-sandbox');

// Create driver instance with headless Chrome options
const driver = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();

const URL = process.env.URL;
// const URL = 'http://akvelon.com/';

async function testCaseHeadless() {
  try {
    await driver.get(URL);

    await driver.findElement(By.id('ufw_1')).click();

    // Switch to iframe
    const iframeElement = await driver.findElement(By.css('iframe'));
    await driver.switchTo().frame(iframeElement);

    // send test message
    const inputElement = await findElementByLocator(driver, By.id('field-:r0:'));

    await inputElement.sendKeys('2 + 2 = ?');

    await driver.sleep(8000);

    // submit message
    await inputElement.submit();

    await driver.sleep(15000);

    // get last answer
    const allAnswers = await findElementByLocators(driver, By.css('.css-mucabg'));
    const textElement = await allAnswers[allAnswers.length - 1].findElement(By.css('p'));
    const text = await textElement.getText();

    console.log(text);

    // validate answer
    const answer = 'The answer is 4.';
    if (text === answer) {
      console.log('Answer is correct');
    } else {
      throw new Error('Incorrect answer');
    }
  } catch (error) {
    console.log('Error:', error.message);
  } finally {
    await driver.quit();
  }
}

async function findElementByLocator(driver, locator) {
  try {
    await driver.wait(until.elementLocated(locator), 5000);
    return driver.findElement(locator);
  } catch (error) {
    console.log('Error locating element:', locator.toString());
    throw error;
  }
}

async function findElementByLocators(driver, locator) {
  try {
    await driver.wait(until.elementsLocated(locator), 5000);
    return driver.findElements(locator);
  } catch (error) {
    console.log(error.message);
    throw error;
  }
}

testCaseHeadless();

const { By, Builder, until } = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');

// Set Chrome options to run in headless mode
const chromeOptions = new chrome.Options();
chromeOptions.addArguments('--headless');
chromeOptions.addArguments('--no-sandbox');

// Provide the path to the manually downloaded ChromeDriver binary
const chromeDriverPath = 'chromedriver.exe';

// Create driver instance with headless Chrome options and specified binary path
const driver = new Builder()
  .forBrowser('chrome')
  .setChromeOptions(chromeOptions)
  .setChromeService(new chrome.ServiceBuilder(chromeDriverPath))
  .build();

async function openYouTube() {
  try {
    // Open YouTube
    await driver.get('https://www.youtube.com');

    // Wait for the page to load completely
    await driver.wait(until.titleContains('YouTube'), 5000);

    // Print the page title
    const title = await driver.getTitle();
    console.log('Page Title:', title);
  } catch (error) {
    console.log('Error:', error.message);
  } finally {
    // Quit the driver
    await driver.quit();
  }
}

openYouTube();

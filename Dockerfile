# FROM selenium/node-chrome
# WORKDIR /app
# COPY . .
# CMD [ "npm", "test" ]

# FROM selenium/node-chrome

# RUN curl -sL https://deb.nodesource.com/setup_18.x | sudo -E bash -
# RUN sudo apt-get install -y nodejs

# WORKDIR /app
# COPY . .
# CMD [ "node", "tests/testHeadlessChrome.js" ]


FROM node:latest

ENV NODE_ENV=production

RUN apt-get update && apt-get install -y firefox-esr

RUN apt-get install -y wget && \
    wget https://github.com/mozilla/geckodriver/releases/download/v0.33.0/geckodriver-v0.33.0-linux64.tar.gz && \
    tar -xzf geckodriver-v0.33.0-linux64.tar.gz && \
    chmod +x geckodriver && \
    mv geckodriver /usr/local/bin/ && \
    rm geckodriver-v0.33.0-linux64.tar.gz

ENV PATH="/usr/local/bin:${PATH}"


WORKDIR /api

COPY . .

RUN npm install --production

# CMD ["node", "tests/testHeadlessFirefox.js"]
CMD ["npm", "test"]